#!/usr/bin/env python3
"""
Description:
    Misc tests
"""
import os
import yaml

# # -----------------------------------------------------------------------------
# # Basic example (https://zetcode.com/python/yaml/)
# # -----------------------------------------------------------------------------
# out_var = [
#   {
#     'name': 'John Doe',
#     'occupation': 'gardener'
#   },
#   {
#     'name': 'Lucy Black',
#     'occupation': 'teacher'
#   }
# ]
# 
# with open('misc.yaml', 'w') as the_writer:
#     data = yaml.dump(out_var, the_writer)
# 
# # misc.yaml:
# # |- nomen: John Doe
# # |  occupation: gardener
# # |- nomen: Lucy Black
# # |  occupation: teacher

# # -----------------------------------------------------------------------------
# # Mimic what Gitlab child CI expects:
# # |variables:
# # |  CHUNKS_SOURCE: "/some/path"
# # -----------------------------------------------------------------------------
# out_var = {
#     "variables": {
#         "CHUNKS_SOURCE": "/path/to/file"
#     }
# }
# 
# with open("misc.yaml", "w") as the_writer:
#     data = yaml.dump(out_var, the_writer)
# 
# # misc.yaml:
# # |variables:
# # |  CHUNKS_SOURCE: /path/to/file

# # -----------------------------------------------------------------------------
# # Create a dynamic pipeline file
# # -----------------------------------------------------------------------------
# project_prefix = "vm-devel_disk"
# valid_ext1 = [ "qcow2", "vdi" ]
# valid_ext2 = [ "zip", "z01", "z02", "z03", "z04" ]
# 
# os.remove("misc.yaml")
# for ext1 in valid_ext1:
#     for ext2 in valid_ext2:
#         #
#         out_var = {
#             ext1 + "." + ext2: {
#                 "stage": "Chunks uploader",
#                 "trigger": {
#                     "project": project_prefix + "/" + ext1 + "/" + ext2,
#                     "branch": "master"
#                 }
#             }
#         }
#         with open("misc.yaml", "a") as the_writer:
#             data = yaml.dump(out_var, the_writer)
#             the_writer.write("\n")
# 
# # misc.yaml:
# # |qcow2.zip:
# # |  stage: Chunks uploader
# # |  trigger:
# # |    branch: master
# # |    project: vm-devel_disk/qcow2/zip
# # | ...

# # -----------------------------------------------------------------------------
# # Create a dynamic pipeline file
# # -----------------------------------------------------------------------------
# project_prefix = "vm-devel_disk"
# valid_ext1 = [ "qcow2", "vdi" ]
# valid_ext2 = [ "zip", "z01", "z02", "z03", "z04" ]
# os.remove("misc.yaml")
# # Output var: Static content
# out_var = {
#     "stages": [ "Upload chunk" ]
# }
# # Output var: Composed content
# for ext1 in valid_ext1:
#     for ext2 in valid_ext2:
#         #
#         out_var[ext1 + "." + ext2] = {
#             "stage": "Upload chunk",
#             "trigger": {
#                 "project": project_prefix + "/" + ext1 + "/" + ext2,
#                 "branch": "master"
#             }
#         }
# # Output var: Save to file
# with open("misc.yaml", "a") as the_writer:
#     data = yaml.dump(out_var, the_writer)
# 
# # misc.yaml:
# # |stages:
# # |- Upload chunk
# # |vdi.z01:
# # |  stage: Upload chunk
# # |  trigger:
# # |    branch: master
# # |    project: vm-devel_disk/vdi/z01
# # | ...
