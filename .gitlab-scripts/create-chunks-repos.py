#!/usr/bin/env python3
"""
Description:
    Use Gitlab API to create repositories to hold VM image chunks using Pages
    Environment variables used here:
      - GITLAB_PAT = personal access token from a user (or CI job token)
      - LTS_FILES_PATH = "/home/gitlab-runner/lts-files"
      - LTS_VM_IMAGE_BASE_NAME = "vm-devel_disk"
      - CI_PROJECT_NAMESPACE = "aiayua/test-ris"
      - LTS_VM_IMAGE_CHUNKS_FILE = "chunks-repositories.txt"
Usage:
    ./create-chunks-repos.py [<a>]
"""
import os
import time
import shutil
import fnmatch
import subprocess
import gitlab
import yaml
from urllib.parse import quote

# Variables
gitlab_pat = os.environ.get("GITLAB_PAT") or "put-your-token-here"
lts_files_path = os.environ.get("LTS_FILES_PATH") or "./images"
lts_base_name = os.environ.get("LTS_VM_IMAGE_BASE_NAME") or "vm-devel_disk"
prj_namespace = os.environ.get("CI_PROJECT_NAMESPACE") or "aiayua/test-ris"
list_file = os.environ.get("LTS_VM_IMAGE_CHUNKS_FILE") or "chunks-repositories.txt"

# Little debug
print("gitlab_pat ......: " + gitlab_pat)
print("lts_files_path ..: " + lts_files_path)
print("lts_base_name ...: " + lts_base_name)
print("prj_namespace ...: " + prj_namespace)
print("list_file .......: " + list_file)

# Use PAT (job token has not enough permissions). received via environment
try:
    gl = gitlab.Gitlab("https://gitlab.com", private_token=gitlab_pat)
except Exception as e:
    exit("Stopping now (" + e.args[0] + ")")

# Get parent group
try:
    grp_parent = gl.groups.get(quote(prj_namespace), safe = "")
except Exception as e:
    exit("Stopping now (" + e.args[0] + ")")

# Clean: If base group exists, delete it (and everything under it)
deletion_timeout = 0
try:
    grp_base = gl.groups.get(quote(prj_namespace + "/" + lts_base_name, safe = ""))
    grp_base.delete()
    print("Deleted '" + lts_base_name + "' group (queued)")
    print("Waiting the deletion to complete")
    while (deletion_timeout < 60):
        print("Checking ...")
        grp_base = gl.groups.get(quote(prj_namespace + "/" + lts_base_name, safe = ""))
        time.sleep(1)
        deletion_timeout += 1;
except Exception as e:
    print("Group '" + lts_base_name + "' does not exist. Seconds spent: " + str(deletion_timeout))

# Create base group
try:
    grp_base = gl.groups.create({"name": lts_base_name, "path": lts_base_name, "parent_id": grp_parent.id})
    print("Created '" + lts_base_name + "' group")
except Exception as e:
    exit("Stopping now (" + e.args[0] + ")")

# Create required repositories, one per final file of < 1 Gb
valid_extensions = [ "qcow2", "vdi" ]
(_, _, files) = next(os.walk(lts_files_path))

# Child pipeline content: Static content
child_pipeline_content = {
    "stages": [ "Upload chunk" ]
}

# We have a few available choices of extensions
for extension in valid_extensions:
    # Create group per extension (qcow2, vdi, ..)
    try:
        grp_extension = gl.groups.create({"name": extension, "path": extension, "parent_id": grp_base.id})
        print("Created '" + lts_base_name + "/" + extension + "' group")
    except Exception as e:
        print("Cannot create group: " + e.args[0])
        print("Continue with next extension")
        continue
    # Get filenames matching this extension
    for filename in files:
        if (fnmatch.fnmatch(filename, lts_base_name + "." + extension + ".z*")):
            # Debug and vars
            print(filename)
            z_extension = filename.split('.')[-1]
            prj_namespace_bits = prj_namespace.split('/')
            subdomain = prj_namespace_bits[0]
            url_prefix = ""
            if len(prj_namespace_bits) > 1:
                prj_namespace_bits.pop(0)
                url_prefix = '/'.join(prj_namespace_bits) + "/"
            url_prefix = url_prefix + lts_base_name + "/" + extension + "/" + z_extension
            child_prj_url = prj_namespace + "/" + lts_base_name + "/" + extension + "/" + z_extension
            child_CI_config_vars = {
                "variables": {
                    "CHUNKS_SOURCE": lts_files_path + "/" + lts_base_name + "." + extension + "." + z_extension
                }
            }

            # Create project per sub-extension (z01, z02, .. , zip)
            try:
                grp_subextension = gl.projects.create({"path": z_extension, "namespace_id": grp_extension.id, "pages_access_level": "public"})
                print("Created '" + lts_base_name + "/" + extension + "/" + z_extension +"' project")
            except Exception as e:
                print("Cannot create project: " + e.args[0])
                print("Continue with next project")
                continue

            # Once the project is created, create its content
            cwd = os.getcwd()
            try:
                if os.path.exists(cwd + "/tmp_folder"):
                    shutil.rmtree(cwd + "/tmp_folder")
                os.mkdir(cwd + "/tmp_folder")
                shutil.copyfile(".gitlab-ci.chunks.yml", cwd + "/tmp_folder/.gitlab-ci.yml")
                with open(cwd + "/tmp_folder/oh-my-vars.yml", "w") as var_writer:
                    data = yaml.dump(child_CI_config_vars, var_writer)
            except Exception as e:
                print("Cannot create git content folder: " + e.args[0])
                print("Continue with next project")
                continue

            # Now we proceed to update the repository
            os.chdir(cwd + "/tmp_folder")
            try:
                subprocess.run(["git", "init", "."])
                subprocess.run(["git", "add", "."])
                subprocess.run(["git", "status"])
                subprocess.run(["git", "commit", "-m", "Committed [" + os.environ.get("CI_PIPELINE_ID") + "][" + os.environ.get("CI_JOB_ID") + "]"])
                subprocess.run(["git", "push", "--all", "--force", "https://oauth2:" + gitlab_pat + "@gitlab.com/" + child_prj_url + ".git"])
                subprocess.run(["git", "status"])
            except Exception as e:
                print("Cannot create git content folder: " + e.args[0])
                print("Continue with next project")
                continue
            finally:
                os.chdir(cwd)
                shutil.rmtree(cwd + "/tmp_folder")

            # Store each future permalink
            #   > https://laboratorio-ris.gitlab.io/vm-devel_disk/vdi/z01
            with open(cwd + "/permalinks.txt", "a") as var_writer:
                var_writer.write("https://" + subdomain + ".gitlab.io/" + url_prefix + "/" + lts_base_name + "." + extension + "." + z_extension + "\n")

            # Child pipeline content: Create child pipelines with multi-project approach
            # - https://docs.gitlab.com/ee/ci/yaml/README.html#trigger-child-pipeline-with-generated-configuration-file
            # - https://docs.gitlab.com/ee/ci/multi_project_pipelines.html
            child_pipeline_content[extension + "." + z_extension] = {
                "stage": "Upload chunk",
                "trigger": {
                    "project": child_prj_url,
                    "branch": "master",
                    "strategy": "depend"
                }
            }

# Child pipeline content: Save to file
try:
    with open(cwd + "/chunk-uploaders.yaml", "w") as the_writer:
        data = yaml.dump(child_pipeline_content, the_writer)
except Exception as e:
    print("Cannot create uploaders YAML file")
    exit("Stopping now (" + e.args[0] + ")")
