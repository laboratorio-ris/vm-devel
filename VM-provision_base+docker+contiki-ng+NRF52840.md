# Virtual machine preparation

The following document describes all operations performed over the virtual machine in order to get a fully working development environment.

Each section will be correspond to a snapshot, so rolling back or clone from snapshot is well described and easy.

Once all tasks of a section are finished, the VM will be shut off and the snapshot will be taken. Text between brackets will be use as name, while the full section will be pasted as description.

## [base-system] Virtual machine base system

OS installation and common customization.

Machine specs:

- CPU: 2
- RAM: 2 Gb (less will force close some programs)
- Disco: 32 Gb (qcow2)
- Red: virbr, NAT defaults

Installer image ISO: https://cdimage.ubuntu.com/lubuntu/releases/20.04.1/release/lubuntu-20.04.1-desktop-amd64.iso

- User name: VM User
- Login: vmu
- Password: vmu
- Hostname: vm-devel
- Log in automatically without asking for the password

Once installed, reboot. The first boot will warn about upgrades, so perform them all and reboot again.

To fit future use, and assuming there will be laptop displays involved, the next customizations are modified by hand:

- Desktop background
- Desktop icon size to 36x36
- Desktop font size to 10 pt
- Monitor to 1440x900
- 4 desktops to 1
- Bottom bar out desktops, in terminal
- Terminal 10 pt, theme Linux
- Added ES keyboard layout on top of US one
- No screensaver
- 10 sec. notifications, no history

```bash
apt-get -y update;
apt-get -y upgrade;
apt-get -y dist-upgrade;
apt-get -y install openssh-server;
apt-get -y autoremove;
```

## [with-docker] Install Docker ecosystem

Following: https://docs.docker.com/engine/install/ubuntu/

```bash
sudo apt-get -y remove docker docker.io containerd runc;
sudo apt-get -y purge docker-ce docker-ce-cli containerd.io;
sudo rm -rf /var/lib/docker;
sudo apt-get -y update;
sudo apt-get -y install apt-transport-https ca-certificates curl gnupg-agent software-properties-common;
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -;
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable";
sudo apt-get -y update;
sudo apt-get -y install docker-ce docker-ce-cli containerd.io;
sudo docker run hello-world;
```

Following: https://docs.docker.com/engine/install/linux-postinstall/

```bash
sudo groupadd docker;
sudo usermod -aG docker $USER;
newgrp docker;
docker run hello-world;
echo "Previous command should work, otherwise check what is wrong before continue";
```

## [docker-contiki] Install latest Contiki-NG, using custom Docker

Based on https://github.com/contiki-ng/contiki-ng/wiki/Docker but using custom repository and image:

```bash
mkdir -pv "${HOME}/work/contiki-ng";
cd "${HOME}/work";
docker login -u vmu -p dqoAp-NdCsrNYKxyAGyX registry.gitlab.com;
docker pull registry.gitlab.com/laboratorio-ris/contiki-ng:latest;
```

Appended to `${HOME}/.bashrc`:

```bash
# Add contiki 'contiker' alias
export CNG_PATH="${HOME}/work/contiki-ng";
contiker()
{
  docker run \
    --privileged \
    --sysctl net.ipv6.conf.all.disable_ipv6=0 \
    --mount type=bind,source=$CNG_PATH,destination=/home/user/contiki-ng \
    -e DISPLAY=$DISPLAY \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v /dev/bus/usb:/dev/bus/usb \
    -ti registry.gitlab.com/laboratorio-ris/contiki-ng:latest \
  ;
};
```

To see if everything is working, we try to flash the 2 different NRF52840 dongles with the `hello_world` example:

> NOTE: Ensure valid connectivity from dongle to docker:
> 
> - Windows -> VM -> docker (contiker)
> - Linux -> VM -> docker (contiker)
> - Linux -> docker (contiker)
> 
> - Is VM/docker getting access to this specific USB device?
>   - If DFU -> 1915:521f "Open DFU Bootloader"
>   - If VCOM -> 1915:520f "nRF52 USB CDC on Contiki-NG"

It is needed the dongle to be in DFU mode:

- Makerdiary dongle: Unplug dongle, press and hold USR/RST button, plug it, wait 2/3 seconds and release the button. If the led is fading red, it is in DFU mode
- Nordic dongle: No need to unplug, just press the RST button. Led will fade to red and the device is in DFU mode.

```bash
contiker
cd examples/hello-world/
make hello-world.dfu-upload
```

If success, it will print some warnings finishing with:

```bash
Zip created at build/nrf52840/dongle/nrf52840_dfu_image.zip
nrfutil dfu usb-serial -p /dev/ttyACM0 -pkg build/nrf52840/dongle/nrf52840_dfu_image.zip
  [####################################]  100%
Device programmed.
```

To test VCOM connection after flashing the example:

```bash
picocom -fh -b 115200 --imap lfcrlf /dev/ttyACM0
```

If it is received the string `Hello, world` periodically, everything went fine. To close picocom press `Ctrl + a`, then `Ctrl + x`.

## [tweaks] Minor changes after some usage

- Clean and configure Firefox to open last opened tabs
- Set Firefox Home to our page
- Added 'cheatsheet.txt' to Desktop

## [latest] Changes not meant to be snapshotted

Each user has to download its own copy, via `fork`, and work directly on it.

```bash
cd "${HOME}/work";
git clone https://gitlab.com/laboratorio-ris/work/<YOUR-REPO>.git ${HOME}/work/contiki-ng/;
```

Enter credentials

```bash
cd "${HOME}/work/contiki-ng";
git submodule update --init --recursive;
```
