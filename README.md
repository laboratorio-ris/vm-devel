# Virtual Machine: `vm-devel`

- Username: `vmu`
- Password: `vmu`

## Specs

This virtual machine has been tested with the following manager and specs_

Virtualbox on Windows:

- Linux OS
- Ubuntu 64 bits
- 2048 Mb RAM
- 2 Cores (same as host)
- Chipset: ICH9
- Paravirtualization: KVM
- Hard-disk driver: AHCI (SATA)
- VGA driver: VMSVGA

KVM(libvirt) on Linux:

- ubuntu20.04 OS
- 2048 Mb RAM
- 2 Cores (copied from host)
- Hard-disk driver: virtio
- VGA driver: virtio
- Network driver: virtio

## Download links

VDI Format (Windows/Linux Virtualbox)

- [vm-devel_disk.vdi.zip](https://laboratorio-ris.gitlab.io/vm-devel_disk/vdi/zip/vm-devel_disk.vdi.zip)
- [vm-devel_disk.vdi.z01](https://laboratorio-ris.gitlab.io/vm-devel_disk/vdi/z01/vm-devel_disk.vdi.z01)
- [vm-devel_disk.vdi.z02](https://laboratorio-ris.gitlab.io/vm-devel_disk/vdi/z02/vm-devel_disk.vdi.z02)
- [vm-devel_disk.vdi.z03](https://laboratorio-ris.gitlab.io/vm-devel_disk/vdi/z03/vm-devel_disk.vdi.z03)
- [vm-devel_disk.vdi.z04](https://laboratorio-ris.gitlab.io/vm-devel_disk/vdi/z04/vm-devel_disk.vdi.z04)
- [vm-devel_disk.vdi.z05](https://laboratorio-ris.gitlab.io/vm-devel_disk/vdi/z05/vm-devel_disk.vdi.z05)
- [vm-devel_disk.vdi.z06](https://laboratorio-ris.gitlab.io/vm-devel_disk/vdi/z06/vm-devel_disk.vdi.z06)

QCOW2 Format (Linux oriented: libvirt, KVM)

- [vm-devel_disk.qcow2.zip](https://laboratorio-ris.gitlab.io/vm-devel_disk/qcow2/zip/vm-devel_disk.qcow2.zip)
- [vm-devel_disk.qcow2.z01](https://laboratorio-ris.gitlab.io/vm-devel_disk/qcow2/z01/vm-devel_disk.qcow2.z01)
- [vm-devel_disk.qcow2.z02](https://laboratorio-ris.gitlab.io/vm-devel_disk/qcow2/z02/vm-devel_disk.qcow2.z02)
- [vm-devel_disk.qcow2.z03](https://laboratorio-ris.gitlab.io/vm-devel_disk/qcow2/z03/vm-devel_disk.qcow2.z03)
- [vm-devel_disk.qcow2.z04](https://laboratorio-ris.gitlab.io/vm-devel_disk/qcow2/z04/vm-devel_disk.qcow2.z04)

## Other links

- [Steps to reproduce `vm-devel`](VM-provision_base+docker+contiki-ng+NRF52840.md)
- [libvirt `vm-devel` XML domain file](libvirt_vm-devel.xml)
